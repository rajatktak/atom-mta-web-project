var express = require("express");
var app = express();
var port = 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/atom");
var nameSchema = new mongoose.Schema({

    EmailID: String,


});
var User = mongoose.model("register1", nameSchema);

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/sign up.html");
});

app.post("/addname", (req, res) => {
    var myData = new User(req.body);
    myData.save()
        .then(item => {
          res.sendFile(__dirname + "/login.html");
        })
        .catch(err => {
            res.sendFile(__dirname + "/error.html");
        });
});

app.listen(port, () => {
    console.log("Server listening on port " + port);
});
